﻿using EFCoreWorkshop.Dal;
using EFCoreWorkshop.Dal.Exceptions;
using EFCoreWorkshop.Dal.Models;
using EFCoreWorkshop.Dal.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreWorkshop.App
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var myContext = new MyContext();


           

            if ((await myContext.Database.GetPendingMigrationsAsync()).Any())
                await myContext.Database.MigrateAsync();


            await Task.WhenAll(new Task[] { });

            await Task.Run(() => { 
            
            });

            //var connection = myContext.Database.GetDbConnection();
            //connection.Open();

            //var command = connection.CreateCommand();
            //command.CommandText = "";
            //com

            // Dapper
            //var result = await connection.QueryMultipleAsync("exec SP {0}", new { Id = 1 });
            //var people = results.Read<Person>().ToList();
            //var food = results.Read<Food>().ToList();
            

//            myContext.Database.EnsureDeleted();
//            myContext.Database.EnsureCreated();


//            myContext.Database.ExecuteSqlRaw(@"
//    CREATE VIEW  MyVet.v_Addresses AS SELECT City, Street, ZipCode FROM MyVet.Addresses;
//");


//            myContext.Database.ExecuteSqlRaw(@"
//EXEC ('CREATE PROCEDURE MyVet.CountAllResources
//AS
//BEGIN
//    SET NOCOUNT ON;
//    SELECT
//           (SELECT COUNT(*) from MyVet.Owners) as Owners,
//           (SELECT COUNT(*) from MyVet.Pieski) as Dogs
//END')
//");



            //            // //Zapis do bazy danych

            var owner = new Owner("Kamil", "Kiełbasa", new Address
            {
                City = "Warszawa",
                Street = "dsadsa",
                ZipCode = "21321"
            })
            {
                //Id = 1,
                //Id = OwnerId.Parse(1),
                Animals = new List<Animal> { new Dog { Name = "Reksio" }, new Rabbit { Name = "Stefan", Breed = "dnsakjl" } },
                CovidPassport = new CovidPassport { Country = "Dadsadas", CovidPassportId = 12, ValidTo = DateTime.UtcNow.AddDays(90) }
            };

            owner.AddAddress(new Address
            {
                City = "Warszawa",
                Street = "dsadsa",
                ZipCode = "21321"
            });
            owner.AddAddress(new Address
            {
                City = "Lubin",
                Street = "Lorem",
                ZipCode = "232133"
            });

            //owner.Address.Add(new Address());

            //var dog = new Dog
            //{
            //    DogNumber = 12
            //};

            //var oId = new OwnerId();
            //var dId = new DogId();

            //var service = new Service();

            //service.Execute(owner.Id, dId);

            //service.Execute(dId, oId);

            // myContext.Owners.Add(owner);
            // myContext.SaveChanges();

            var pageNumber = 1;
            var pageSize = 2;

            //var a = myContext.Owners
            //    .Include(x => x.Dogs)
            //    .AsQueryable<Owner>();

            //a = a.OrderByDescending(x => x.CreatedAt);
            //a = a.Where(x => x.FirstName.Contains(Get7().ToString()));
            //a = a.Take(pageSize).Skip(pageNumber * pageSize);

            //var b = a.First();

            //.Include(x => x.Dogs)
            //    .ThenInclude(x => x.Obj)
            //.Take(1);
            //.ToList();
            // First
            // ToListAsync
            // FirstAsync
            //.First(x => x.Id == 1);


            //b.FirstName = "Test";
            //b.Dogs.First().Name = "Dogggo";

            //myContext.SaveChanges();

            //myContext.Owners.Remove(new Owner { Id = 1 });

            //myContext.SaveChanges();

            var repository = new OwnerRepository(myContext);

            //var addresses = repository.Get();

            //var owner = repository.Get(1);

            await repository.Add(owner);

            //repository.Delete(owner);

            try
            {
                myContext.ChangeTracker.DetectChanges();
                Console.WriteLine(myContext.ChangeTracker.DebugView.LongView);

                await repository.UnitOfWork.SaveChangesAsync();
            }
            catch (FirstNameLastNameConstrainViolation e)
            {
                Console.WriteLine("Oh no :( ");
            }

            //repository.Delete(owner);

            await repository.UnitOfWork.SaveChangesAsync();

            //var addresses = myContext.AddressReadModels.FromSqlRaw("SELECT City, Street, ZipCode FROM MyVet.Addresses ORDER BY ZipCode").ToList();

            //var dogs = myContext.Dogs.FromSqlRaw("SELECT * FROM MyVet.Pieski").ToList();

            //var resources = myContext.ResourceCounts.FromSqlInterpolated($"exec MyVet.CountAllResources").ToList();

            //var resources = myContext.ResourceCounts.FromSqlRaw($"exec MyVet.CountAllResources {0}", id).ToList();
            //myContext.ResourceCounts.FromSqlRaw("exec MyVet.CountAllResources " + id);

            //var addresses = myContext.AddressReadModels
            //    .Take(1).Skip(1)
            //    .OrderByDescending(x => x.ZipCode).ToList();

            //var owner = repository.Get(1);

            //owner.FirstName = "Test";

            //repository.Update(owner);

            Console.WriteLine("");
        }

        static int Get7() => 7;
    }

    //public interface IMessageBus
    //{
    //    void Publish(object a);
    //}

    //public class MyHandler
    //{
    //    private readonly OwnerRepository repository;
    //    private readonly IMessageBus bus;

    //    public MyHandler(OwnerRepository repository, IMessageBus bus)
    //    {
    //        this.repository = repository;
    //        this.bus = bus;
    //    }

    //    public void Handle()
    //    {
    //        var entity = this.repository.Get(1);

    //        /// dzialania na encji
    //        /// 
    //        entity.LastName = "mkdsa";

    //        /// zalezy od transkacji
    //        this.repository.UnitOfWork.SaveChanges();

    //        bus.Publish(new { });
    //    }
    //}


    /// Command -> Handler
    /// 

    public class Service
    {
        public async Task Execute()
        {
            await Task.Delay(TimeSpan.FromSeconds(2));
        }
    }

}
