﻿using EFCoreWorkshop.Dal.Helpers;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCoreWorkshop.Dal.Migrations
{
    public partial class AddView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.Sql($"CREATE VIEW {MyContext.Schema}.v_Addresses AS SELECT City, Street, ZipCode FROM MyVet.Addresses");
            migrationBuilder.CreateView(@"SELECT City, Street, ZipCode FROM MyVet.Addresses", "v_Addresses", MyContext.Schema);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.Sql($"DROP VIEW {MyContext.Schema}.v_Addresses");
            migrationBuilder.DropView("v_Addresses", MyContext.Schema);
        }
    }
}
