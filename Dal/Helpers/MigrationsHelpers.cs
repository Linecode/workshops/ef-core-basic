﻿using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using Microsoft.EntityFrameworkCore.Migrations.Operations.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Helpers
{
    public static class MigrationsHelpers
    {
        public static OperationBuilder<SqlOperation> CreateView(this MigrationBuilder migrationBuilder, string query, string name, string scheme = "dbo")
            => migrationBuilder.Sql($"CREATE VIEW {scheme}.{name} as {query}");

        public static OperationBuilder<SqlOperation> DropView(this MigrationBuilder migrationBuilder, string name, string scheme = "dbo")
            => migrationBuilder.Sql($"DROP VIEW {scheme}.{name}");
    }
}
