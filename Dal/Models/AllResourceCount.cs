﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Models
{
    public class AllResourceCount
    {
        public int Dogs { get; set; }
        public int Owners { get; set; }
    }
}
