﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Models
{
    public class DogOwner
    {
        public DateTime CreatedAt { get; set; }

        public int DogId { get; set; }
        public Dog Dog { get; set; }

        public int OwnerId { get; set; }
        public Owner Owner { get; set; }
    }
}
