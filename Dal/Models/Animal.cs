﻿using System.Collections.Generic;

namespace EFCoreWorkshop.Dal.Models
{
    public abstract class Animal
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int OwnerId { get; set; }
        public Owner Owners { get; set; }
    }
}
