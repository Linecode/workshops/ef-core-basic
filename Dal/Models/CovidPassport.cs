﻿using System;

namespace EFCoreWorkshop.Dal.Models
{
    public class CovidPassport
    {
        public int CovidPassportId { get; set; }
        public DateTime ValidTo { get; set; }
        public string Country { get; set; }
    }
}
