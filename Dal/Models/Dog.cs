﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCoreWorkshop.Dal.Models
{
    public class Dog : Animal
    {
        //public int DogNumber { get; set; }
        //public string Name { get; set; }
        //public int OwnerId { get; set; }
        //public Owner Owner { get; set; }
        public DateTime CreatedAt { get; set; }

        //public List<DogOwner> DogOwners { get; set; }

        //public ICollection<Owner> Owners { get; set; } = new List<Owner>();
    }

    public class Rabbit : Animal
    {
        public string Breed { get; set; }
        public int Weight { get; set; }

        public DateTime CreatedAt { get; set; }
    }

    public readonly struct DogId
    {

    }
}
