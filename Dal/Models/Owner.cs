﻿using EFCoreWorkshop.Dal.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCoreWorkshop.Dal.Models
{
    //public class Owner : BaseEntity<OwnerId>
    public class Owner : BaseEntity<int>
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Notes { get; private set; }

        //public List<DogOwner> DogOwners { get; set; }
        public virtual ICollection<Animal> Animals { get; set; }

        public int MyProperty { get; set; }

        public DateTime CreatedAt { get; set; }

        private List<Address> _addresses = new();
        public IReadOnlyCollection<Address> Address => _addresses;

        public OwnerState State { get; private set; } = OwnerState.Pending;

        public List<string> NickNames = new List<string> { "dsadas", "dsandsada" };

        public CovidPassport CovidPassport { get; set; }

        public void AddAddress(Address address)
            => _addresses.Add(address);

        [Obsolete("Only for EF Core", true)]
        private Owner() { }

        public Owner(string firstName, string lastName, Address address)
        {
            FirstName = firstName;
            LastName = lastName;

            _addresses.Add(address);
        }

        public void AddNotes(string notes)
            => Notes = notes;

        public enum OwnerState
        {
            Active,
            Pending,
            Banned 
        }
    }

    // Value Converter
    // x => x.Value
    // x => OwnerId.Prase(x)
    public readonly struct OwnerId
    {
        public int Value { get; }

        private OwnerId(int value)
        {
            Value = value;
        }

        public static OwnerId Parse(int value) => new OwnerId(value);
    }
}
