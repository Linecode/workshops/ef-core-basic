﻿using EFCoreWorkshop.Dal.Exceptions;
using EFCoreWorkshop.Dal.Models;
using EFCoreWorkshop.Dal.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal
{
    public class MyContext : DbContext, IUnitOfWork
    {
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Dog> Dogs { get; set; }
        public DbSet<Rabbit> Rabbits { get; set; }

        // Only for query
        public DbSet<AddressReadModel> AddressReadModels { get; set; }

        // EF 2.2
        //public DbQuery<AddressReadModel> AddressReadModel { get; set; }

        // Onfly for SP
        public DbSet<AllResourceCount> ResourceCounts { get; set; }

        public MyContext()
        {

        }

        public MyContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(Settings.ConnectionString);
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.ApplyConfiguration(new OwnerEntityTypeConfiguration());

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MyContext).Assembly);

            //modelBuilder.Entity<AllResourceCount>().HasNoKey().ToView(null);

            //modelBuilder.Entity<AddressReadModel>().HasNoKey();
            //modelBuilder.Ignore<AddressReadModel>();

            //modelBuilder.Entity<Dog>()
            //    .HasKey(x => x.DogNumber);
            //modelBuilder.Entity<Dog>()
            //    .Property(x => x.Name)
            //    .HasMaxLength(255)
            //    .IsRequired(true);
            //modelBuilder.Entity<Dog>()
            //    .Property(x => x.OwnerId)
            //    .IsRequired();

            //modelBuilder.Entity<Owner>()
            //    .HasKey(x => x.Id);
            //modelBuilder.Entity<Owner>()
            //    .Property(x => x.FirstName)
            //    .HasMaxLength(255)
            //    .IsRequired(true);
            //modelBuilder.Entity<Owner>()
            //   .Property(x => x.LastName)
            //   .HasMaxLength(255)
            //   .IsRequired(true);
            //modelBuilder.Entity<Owner>()
            //    .Property(x => x.Notes)
            //    .HasMaxLength(10_000);

            //foreach (var entity in modelBuilder.Model.GetEntityTypes())
            //{
            //    foreach (var prop in entity.GetProperties())
            //    {
            //        if (prop.ClrType == typeof(string))
            //        {
            //            if (prop.GetMaxLength() == null)
            //            {
            //                prop.SetMaxLength(100);
            //            }
            //            prop.IsNullable = false;
            //        }
            //    }
            //}
        }

        public override int SaveChanges()
        {
            foreach (var item in ChangeTracker.Entries())
            {
                if (item.State == EntityState.Added)
                {
                    item.CurrentValues["CreatedAt"] = DateTime.UtcNow;
                }

                //if (item.State == EntityState.Deleted)
                //{
                //    var member = item.Members.FirstOrDefault(x => x.Metadata.Name == "DeletedAt");

                //    if (member != null)
                //    {
                //        item.CurrentValues["IsDeleted"] = true;
                //        item.CurrentValues["DeletedAt"] = DateTime.UtcNow;

                //        item.State = EntityState.Modified;
                //    }
                //}
            }

            try
            {
                return base.SaveChanges();
            }
            catch (Exception e)
            {
                static bool IsConstrainViolation(Exception e, string constrainName)
                    => e.InnerException != null && e.InnerException.Message.Contains(constrainName);

                if (IsConstrainViolation(e, "IX_Owners_FirstName_LastName"))
                    throw new FirstNameLastNameConstrainViolation("", e);

                throw;
            }
        }

        public override async Task<int> SaveChangesAsync()
        {
            foreach (var item in ChangeTracker.Entries())
            {
                if (item.State == EntityState.Added)
                {
                    item.CurrentValues["CreatedAt"] = DateTime.UtcNow;
                }

                //if (item.State == EntityState.Deleted)
                //{
                //    var member = item.Members.FirstOrDefault(x => x.Metadata.Name == "DeletedAt");

                //    if (member != null)
                //    {
                //        item.CurrentValues["IsDeleted"] = true;
                //        item.CurrentValues["DeletedAt"] = DateTime.UtcNow;

                //        item.State = EntityState.Modified;
                //    }
                //}
            }

            try
            {
                return await base.SaveChangesAsync(CancellationToken.None);
            }
            catch (Exception e)
            {
                static bool IsConstrainViolation(Exception e, string constrainName)
                    => e.InnerException != null && e.InnerException.Message.Contains(constrainName);

                if (IsConstrainViolation(e, "IX_Owners_FirstName_LastName"))
                    throw new FirstNameLastNameConstrainViolation("", e);

                throw;
            }
        }

        static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

        public static string Schema => "MyVet";
    }
}
