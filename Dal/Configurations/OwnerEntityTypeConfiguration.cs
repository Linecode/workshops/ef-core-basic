﻿using EFCoreWorkshop.Dal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EFCoreWorkshop.Dal.Configurations
{
    public class OwnerEntityTypeConfiguration : IEntityTypeConfiguration<Owner>
    {
        public void Configure(EntityTypeBuilder<Owner> owner)
        {
            owner
                .HasKey(x => x.Id);

            //owner.Property(x => x.Id)
            //  .UseHiLo("ownersseq");

            owner
                .Property(x => x.FirstName)
                .HasMaxLength(255)
                .IsRequired(true);
            owner
               .Property(x => x.LastName)
               .HasMaxLength(255)
               .IsRequired(true);
            owner
                .Property(x => x.Notes)
                .HasMaxLength(10_000);

            owner.Ignore(x => x.MyProperty);

            owner.Property(x => x.CreatedAt)
                .HasPrecision(1);
            //.HasDefaultValueSql("GETDATE()");

            owner.Property(x => x.State)
                .HasConversion<string>();

            owner.Property(x => x.NickNames)
                .HasConversion(new ListToStringConverter());

            owner.Property<bool>("IsDeleted");
            owner.Property<DateTime>("DeletedAt");

            //owner.Ignore(x => x.CovidPassport);


            //owner.HasOne(x => x.Address)
            //    .WithOne(x => x.Owner)
            //    .HasForeignKey<Address>(x => x.OwnerId)
            //    .OnDelete(DeleteBehavior.Cascade);

            //owner.HasMany(x => x.Address)
            //    .WithOne(x => x.Owner)
            //    .HasForeignKey(x => x.OwnerId)
            //    .OnDelete(DeleteBehavior.Cascade);

            //owner.HasMany(x => x.Dogs)
            //    .WithMany(x => x.Owners)
            //    .UsingEntity<DogOwner>(
            //        (dogOwner) => dogOwner.HasOne(x => x.Dog)
            //            .WithMany(x => x.DogOwners)
            //            .OnDelete(DeleteBehavior.Cascade), 
            //        (dogOwner) => dogOwner.HasOne(x => x.Owner)
            //            .WithMany(x => x.DogOwners)
            //            .OnDelete(DeleteBehavior.Cascade), 
            //        (dogOwner) =>
            //        {
            //            dogOwner.Property(x => x.CreatedAt);
            //        });

            owner.OwnsMany(x => x.Address, a =>
            {
                a.HasKey(x => x.Id);

                a.Property(x => x.City);
                a.Property(x => x.Street);
                a.Property(x => x.ZipCode);

                a.ToTable("Addresses", MyContext.Schema);
            });

            owner.OwnsOne(x => x.CovidPassport, cp => 
            {
                cp.Property(x => x.CovidPassportId).HasColumnName($"{nameof(CovidPassport)}_Id");
                cp.Property(x => x.ValidTo).HasColumnName($"{nameof(CovidPassport)}_ValidTo");
                cp.Property(x => x.Country).HasColumnName($"{nameof(CovidPassport)}_Country");
                cp.Property<DateTime>("CreatedAt");
            });

            owner.Property<DateTime>("LastUpdated")
                .HasDefaultValueSql("GETDATE()")
                .ValueGeneratedOnAddOrUpdate();

            owner.HasIndex(x => new { x.FirstName, x.LastName })
                .IsUnique();

            owner.ToTable(nameof(MyContext.Owners), MyContext.Schema);
        }
    }

    public class ListToStringConverter : ValueConverter<List<string>, string>
    {
        public ListToStringConverter() : base(x => Make(x), x => UnMake(x))
        {

        }

        static string Make(List<string> list) => JsonConvert.SerializeObject(list);
        static List<string> UnMake(string value) => JsonConvert.DeserializeObject<List<string>>(value);
    }
}
