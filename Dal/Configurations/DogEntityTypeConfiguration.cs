﻿using EFCoreWorkshop.Dal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreWorkshop.Dal.Configurations
{
    public class DogEntityTypeConfiguration : IEntityTypeConfiguration<Dog>
    {
        public void Configure(EntityTypeBuilder<Dog> builder)
        {
            //builder
            //    .HasKey(x => x.DogNumber);

            builder
                .Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired(true);
            //builder
            //    .Property(x => x.OwnerId)
            //    .IsRequired();

            builder.Property(x => x.CreatedAt)
                .HasColumnName("CreatedAt");

            //builder.ToTable("Pieski", MyContext.Schema);
        }
    }

    public class AnimalEntityTypeConfiguration : IEntityTypeConfiguration<Animal>
    {
        public void Configure(EntityTypeBuilder<Animal> builder)
        {
            builder.HasDiscriminator<string>("Animal_type")
                .HasValue<Dog>("Dog")
                .HasValue<Rabbit>("Rabbit");

            builder.ToTable("Animals", MyContext.Schema);
        }
    }

    public class RabbitEntityTypeConfiguration : IEntityTypeConfiguration<Rabbit>
    {
        public void Configure(EntityTypeBuilder<Rabbit> builder)
        {
            builder.Property(x => x.CreatedAt)
                .HasColumnName("CreatedAt");
        }
    }
}
