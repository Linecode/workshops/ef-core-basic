﻿using EFCoreWorkshop.Dal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Configurations
{
    //public class AddressEntityTypeConfiguration : IEntityTypeConfiguration<Address>
    //{
    //    public void Configure(EntityTypeBuilder<Address> builder)
    //    {
    //        builder.HasKey(x => x.Id);

    //        builder.Property(x => x.City);
    //        builder.Property(x => x.Street);
    //        builder.Property(x => x.ZipCode);
    //        builder.Property(x => x.OwnerId);

    //        builder.ToTable("Addresses", MyContext.Schema);
    //    }
    //}

    public class AddressReadModelEntityTypeConfiguration : IEntityTypeConfiguration<AddressReadModel>
    {
        public void Configure(EntityTypeBuilder<AddressReadModel> builder)
        {
            builder.HasNoKey();

            builder.ToView(null);
        }
    }
}
