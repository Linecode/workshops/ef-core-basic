﻿using EFCoreWorkshop.Dal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Configurations
{
    public class AllResourcesCountEntityTypeConfiguration : IEntityTypeConfiguration<AllResourceCount>
    {
        public void Configure(EntityTypeBuilder<AllResourceCount> builder)
        {
            builder.HasNoKey()
                .ToView(null);
        }
    }
}
