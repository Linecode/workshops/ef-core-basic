﻿namespace EFCoreWorkshop.Dal.Repositories
{
    public abstract class BaseEntity<K>
    {
        public K Id { get; set; }
    }
}
