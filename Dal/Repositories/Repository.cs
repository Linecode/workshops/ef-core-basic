﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Repositories
{
    public class Repository<T, K> where T : BaseEntity<K>
    {
        protected readonly MyContext _context;
        protected readonly DbSet<T> _entities;

        public IUnitOfWork UnitOfWork => _context;

        public Repository(MyContext context)
        {
            _context = context ?? throw new ArgumentNullException();
            _entities = context.Set<T>();
        }

        public virtual async Task Add(T entity)
        { 
           await _entities.AddAsync(entity); 
        }

        public virtual void Update(T entity)
            => _context.Entry(entity).State = EntityState.Modified;

        public virtual void Delete(T entity)
            => _entities.Remove(entity);

        public virtual async Task<T> Get(K entityId)
        {
            return await _entities.FirstOrDefaultAsync(x => x.Id.Equals(entityId));
        }

        public virtual async Task<List<T>> Get(K[] entitiesIds)
        { 
            return await _entities.Where(x => entitiesIds.Contains(x.Id)).ToListAsync(); 
        }

        public virtual Task<IQueryable<T>> GetQueryable()
        { 
            return Task.FromResult(_entities.AsQueryable()); 
        }
    }
}
