﻿using System.Threading;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Repositories
{
    public interface IUnitOfWork
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }

    //public class OwnerRepository
    //{
    //    private readonly MyContext _context;

    //    public IUnitOfWork UnitOfWork => _context;

    //    public OwnerRepository(MyContext context)
    //    {
    //        _context = context;
    //    }

    //    public Owner Get(int id) {
    //        return _context.Owners
    //            .Include(x => x.Dogs)
    //            .FirstOrDefault(x => x.Id == id);
    //    }
    //    public List<Owner> GetAll()
    //    {
    //        return _context.Owners.ToList();
    //    }

    //    // repository.Get().Where(x => true | false).Take().Skip().ToList()
    //    public IQueryable<Owner> Get()
    //    {
    //        return _context.Owners.AsQueryable();
    //    }

    //    // repository.Get(x => x.IsActive)
    //    //public List<Owner> Get(Func<Owner, bool> predicate, int? pageNumber, int? pageSize)
    //    //{
    //    //    var query = _context.Owners.AsQueryable();

    //    //    query = query.Where(predicate).AsQueryable();

    //    //    if (pageNumber.HasValue && pageSize.HasValue)
    //    //        query = query.Take(pageSize.Value).Skip(pageSize.Value * pageNumber.Value);

    //    //    return query.ToList();
    //    //}

    //    public List<Owner> Get(OwnerGetFilterObject filterObject)
    //    {
    //        var query = _context.Owners.AsQueryable();

    //        if (filterObject != null)
    //            query = query.Where(filterObject.Predicate).AsQueryable();

    //        if (filterObject.PageNumber.HasValue && filterObject.PageSize.HasValue)
    //            query = query.Take(filterObject.PageSize.Value).Skip(filterObject.PageSize.Value * filterObject.PageNumber.Value);

    //        return query.ToList();
    //    }

    //    public void Add(Owner owner) 
    //    {
    //        _context.Owners.Add(owner);
    //    }
    //    public void Remove(Owner owner) 
    //    {
    //        _context.Owners.Remove(owner);
    //    }
    //    public void Update(Owner owner) 
    //    {
    //        _context.Entry(owner).State = EntityState.Modified;
    //    }

    //    public class OwnerGetFilterObject
    //    {
    //        public int? PageNumber { get; set; } = 1;
    //        public int? PageSize { get; set; } = 25;

    //        public Func<Owner, bool> Predicate { get; set; }
    //    }
    //}
}
