﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreWorkshop.Dal.Exceptions
{
    public class FirstNameLastNameConstrainViolation : Exception
    {
        public FirstNameLastNameConstrainViolation()
        {
        }

        public FirstNameLastNameConstrainViolation(string message) : base(message)
        {
        }

        public FirstNameLastNameConstrainViolation(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
